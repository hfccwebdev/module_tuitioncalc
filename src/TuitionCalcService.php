<?php

/**
 * Defines the F4F Payment Calculator Service.
 */
class TuitionCalcService implements TuitionCalcServiceInterface {

  /**
   * Defines the in-district 100-200 level tuition per credit hour.
   */
  const INDISTRICT_ASSOC = 108.00;

  /**
   * Defines the out-of-district 100-200 level tuition per credit hour.
   */
  const OUTDISTRICT_ASSOC = 188.00;

  /**
   * Defines the in-district 100-200 level tuition per credit hour.
   */
  const OUTSTATE_ASSOC = 273.00;

  /**
   * Defines the out-of-district 100-200 level tuition per credit hour.
   */
  const INTL_ASSOC = 273.00;

  /**
   * Defines the in-district 300-400 level tuition per credit hour.
   */
  const INDISTRICT_BACH = 200.00;

  /**
   * Defines the out-of-district 300-400 level tuition per credit hour.
   */
  const OUTDISTRICT_BACH = 265.00;

  /**
   * Defines the in-district 300-400 level tuition per credit hour.
   */
  const OUTSTATE_BACH = 350.00;

  /**
   * Defines the out-of-district 300-400 level tuition per credit hour.
   */
  const INTL_BACH = 350.00;

  /**
   * Defines the per-semester registration fee.
   */
  const REGFEE = 50.00;

  /**
   * Defines the per-semester infrastructure fee.
   */
  const INFRFEE = 60.00;

  /**
   * Defines the per-credit-hour technology fee.
   */
  const TECHFEE = 4.00;

  /**
   * Defines the per-credit-hour service fee.
   */
  const SERFEE = 18.00;

  /**
   * Defines the per-credit-hour student activity fee.
   */
  const ACTFEE = 2.00;

  /**
   * Stores the Web Services Client service.
   *
   * @var \WebServicesClientInterface
   */
  private $webServicesClient;

  /**
   * Stores the HFC HANK API Client.
   *
   * @var \HfcHankApiInterface
   */
  private $hfcHankApi;

  /**
   * Stores the HANK API Fees tables.
   *
   * @var string[]
   */
  private static $sectionFeesTable = [];

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static(
      WebServicesClient::create(),
      HfcHankApi::create()
    );
  }

  /**
   * Initialize the Tuition Calculator Service object.
   *
   * @param \WebServicesClientInterface $web_services_client
   *   The Web Services Client to retrieve HFC Catalog data.
   * @param \HfcHankApiInterface $hfc_hank_api
   *   The HFC HANK API Client to retrieve HANK API data.
   */
  public function __construct(
    WebServicesClientInterface $web_services_client,
    HfcHankApiInterface $hfc_hank_api
  ) {
    $this->webServicesClient = $web_services_client;
    $this->hfcHankApi = $hfc_hank_api;
  }

  /**
   * {@inheritdoc}
   */
  public function calculate(
    string $residency,
    float $credit_hours_assoc,
    float $credit_hours_bach = 0,
    float $course_fees = NULL,
    float $excess_fees = NULL,
    float $bookstore_fees = NULL
  ): array {

    switch ($residency) {
      case 'indist':
        $tuition_assoc = self::INDISTRICT_ASSOC;
        $tuition_bach = self::INDISTRICT_BACH;
        break;

      case 'outdist':
        $tuition_assoc = self::OUTDISTRICT_ASSOC;
        $tuition_bach = self::OUTDISTRICT_BACH;
        break;

      case 'outstate':
        $tuition_assoc = self::OUTSTATE_ASSOC;
        $tuition_bach = self::OUTSTATE_BACH;
        break;

      case 'intl':
        $tuition_assoc = self::INTL_ASSOC;
        $tuition_bach = self::INTL_BACH;
        break;

      default:
        drupal_set_message(
          t('Unknown residency %res. Cannot calculate results', ['%res' => $residency]),
          'error'
        );
        return [];
    }

    $tuition_total = $tuition_assoc * $credit_hours_assoc + $tuition_bach * $credit_hours_bach;

    $total_credit_hours = $credit_hours_assoc + $credit_hours_bach;

    $regfee = self::REGFEE;
    $inffee = self::INFRFEE;
    $techfee = self::TECHFEE * $total_credit_hours;
    $svcfee = self::SERFEE * $total_credit_hours;
    $actfee = self::ACTFEE * $total_credit_hours;

    $tuition_and_fees = $tuition_total + $regfee + $inffee + $techfee + $svcfee + $actfee;

    $total = $tuition_and_fees + $course_fees + $excess_fees + $bookstore_fees;

    return [
      'tuition_amount_assoc' => $tuition_assoc,
      'tuition_amount_bach' => $tuition_bach,
      'tuition_total' => $tuition_total,
      'registration_fee' => $regfee,
      'infrastructure_fee' => $inffee,
      'technology_fee' => $techfee,
      'service_fee' => $svcfee,
      'activity_fee' => $actfee,
      'tuition_and_fees' => $tuition_and_fees,
      'course_fees' => $course_fees,
      'excess_contact_hours_fees' => $excess_fees,
      'bookstore_fees' => $bookstore_fees,
      'total_charges' => $total,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateAllSections(string $residency, string $sec_term, array $sections): array {

    $credit_hours_assoc = 0.00;
    $credit_hours_bach = 0.00;
    $course_fees = 0.00;
    $excess_fees = 0.00;
    $bookstore_fees = 0.00;
    $section_detail = [];

    foreach ($sections as $section) {
      $fee_info = $this->getSectionFees($sec_term, $section['course_sections_id']);

      if (intval($section['sec_course_no']) < 300) {
        $credit_hours_assoc += $section['crs_min_cred'];
      }
      else {
        $credit_hours_bach += $section['crs_min_cred'];
      }

      $excess = ($residency == 'indist')
        ? $fee_info->EXCESS_CONTACT_HOURS_FEE_ID
        : $fee_info->EXCESS_CONTACT_HOURS_FEE_OD;

      $course_fees += $fee_info->COURSE_FEE;
      $excess_fees += $excess;
      $bookstore_fees += $fee_info->BOOK_FEE;

      $section_detail[] = [
        'sec_name' => $section['crs_name'] . '-' . $section['sec_no'],
        'sec_short_title' => $section['sec_short_title'],
        'crs_min_cred' => $section['crs_min_cred'],
        'course_fee' => $fee_info->COURSE_FEE,
        'excess_contact_hours_fee' => $excess,
        'bookstore_fees' => $fee_info->BOOK_FEE,
      ];
    }

    $result = [
      'credit_hours_assoc' => $credit_hours_assoc,
      'credit_hours_bach' => $credit_hours_bach,
      'section_detail' => $section_detail,
    ];

    $result += $this->calculate(
      $residency,
      $credit_hours_assoc,
      $credit_hours_bach,
      $course_fees,
      $excess_fees,
      $bookstore_fees
    );

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSectionInfo(array $section) {

    // WebServicesClient filter opts are counter-intuitive.
    // @see https://dvc.hfcc.net/webadmin/issues/issue8189
    $filter_opts = [];
    foreach ($section as $key => $value) {
      $filter_opts[] = "$key=$value";
    }

    $result = $this->webServicesClient->getCourseSections($filter_opts);

    if (!empty($result) && (count($result) === 1)) {
      return reset($result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSectionFees(string $sec_term, string $course_sections_id) {
    $fees = $this->getTermFees($sec_term);
    return $fees[$course_sections_id] ?: [];
  }

  /**
   * Builds a static array of HANK Course Section Fees.
   *
   * This process adds array keys necessary for meaningful lookups
   * that are not provided directly by the HANK API.
   *
   * @param string $sec_term
   *   The academic term to query.
   *
   * @return string[]
   *   An array of fee information for the given term.
   */
  private function getTermFees($sec_term) {
    $sec_term = str_replace('/', '', $sec_term);
    if (empty(static::$sectionFeesTable[$sec_term])) {
      $term_data = $this->hfcHankApi->getData('course-sections-fees', $sec_term, [], TRUE, NULL, 30) ?: NULL;
      if (!empty($term_data)) {
        foreach ($term_data as $section) {
          static::$sectionFeesTable[$sec_term][$section->COURSE_SECTIONS_ID] = $section;
        }
      }
    }
    return static::$sectionFeesTable[$sec_term];
  }

}
