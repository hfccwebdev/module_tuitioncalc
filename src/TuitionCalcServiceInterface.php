<?php

/**
 * Defines the F4F Payment Calculator Service Interface.
 */
interface TuitionCalcServiceInterface {

  /**
   * Creates an instance of this class.
   */
  public static function create();

  /**
   * Calculates total tuition and fees.
   *
   * @param string $residency
   *   Determine the applicable residency.
   *   - indist : In-district.
   *   - outdist : Out-of-district.
   *   - outstate: Out of state.
   *   - intl: International.
   * @param float $credit_hours_assoc
   *   The total 100-200 level credit hours taken for the semester.
   * @param float $credit_hours_bach
   *   The total =300-400 level credit hours taken for the semester.
   * @param float $course_fees
   *   The total of all course fees.
   * @param float $excess_fees
   *   The total of all excess contact hour fees.
   * @param float $bookstore_fees
   *   The total of Inclusive Access Bookstore fees and charges.
   *
   * @return float[]
   *   An array of information about calculated tuition and fees.
   */
  public function calculate(
    string $residency,
    float $credit_hours_assoc,
    float $credit_hours_bach,
    float $course_fees,
    float $excess_fees,
    float $bookstore_fees
  );

  /**
   * Build results from course section info.
   *
   * We need both Catalog Course Sections and HANK API fees results
   * because the fees API does not include credit hours.
   *
   * @param string $residency
   *   Determine the applicable residency.
   *   - indist : In-district.
   *   - outdist : Out-of-district.
   *   - outstate: Out of state.
   *   - intl: International.
   * @param string $sec_term
   *   The Academic Term to query.
   * @param string[] $sections
   *   Sections returned from $this->getSectionInfo().
   *
   * @return array
   *   An array of fee-related information.
   */
  public function calculateAllSections(string $residency, string $sec_term, array $sections);

  /**
   * Find a Course Section info from the Catalog API.
   *
   * @param string[] $section
   *   The course section defined by the following elements:
   *   - sec_subject: Course Subject.
   *   - sec_course_no: Course Number.
   *   - sec_no: Section Number.
   *   - sec_term: Academic term.
   *
   * @return string[]
   *   The HANK Course Section details.
   */
  public function getSectionInfo(array $section);

  /**
   * Gets fee information about a course section.
   *
   * @param string $sec_term
   *   The Academic Term to query.
   * @param string $course_sections_id
   *   The HANK Course Sections ID to locate.
   *
   * @return array
   *   An array of info from the course-sections-fees HANK API endpoint.
   */
  public function getSectionFees(string $sec_term, string $course_sections_id);

}
