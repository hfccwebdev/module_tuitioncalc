<?php

/**
 * Defines the TuitionCalcBlock class.
 */
class TuitionCalcBlock extends HfcGlobalBaseBlock {

  /**
   * {@inheritdoc}
   */
  public function info() {
    return [
      'info' => t('Tuition Calculator Block'),
      'cache' => DRUPAL_NO_CACHE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function build(&$output) {
    $output[] = ['#markup' => tuitioncalc_html_page()];
  }

}
