<?php

/**
 * @file
 * Tuition Calculator module.
 *
 * @ingroup hfcc_modules
 */

/**
 * Implements hook_menu().
 */
function tuitioncalc_menu() {
  return [
    'tuition-and-payment/calculator' => [
      'title' => 'Tuition Estimate Calculator',
      'page callback' => 'tuitioncalc_html_page',
      'access arguments' => ['access content'],
    ],
  ];
}

/**
 * Implements hook_block_info().
 */
function tuitioncalc_block_info() {
  return [
    'tuitioncalc' => TuitionCalcBlock::create()->info(),
  ];
}

/**
 * Implements hook_block_configure().
 */
function tuitioncalc_block_configure($delta = '') {
  switch ($delta) {
    case 'tuitioncalc':
      return TuitionCalcBlock::create()->configure();
  }
}

/**
 * Implements hook_block_save().
 */
function tuitioncalc_block_save($delta = '', $edit = []) {
  switch ($delta) {
    case 'tuitioncalc':
      return TuitionCalcBlock::create()->save($edit);
  }
}

/**
 * Implements hook_block_view().
 */
function tuitioncalc_block_view($delta = '') {
  switch ($delta) {
    case 'tuitioncalc':
      return TuitionCalcBlock::create()->view();
  }
}

/**
 * Implements hook_html_page().
 */
function tuitioncalc_html_page() {
  drupal_add_js(drupal_get_path('module', 'tuitioncalc') . '/js/tuition-calculator.js');
  $output = <<<ENDTEXT
  <form id="tuition-calc">
    <fieldset>
      <div class="form-item">
        <label for="term">Term</label>
        <select id="term">
          <!-- option value="winter" selected>Winter 2024</option -->
          <option value="spsu">Summer 2024</option>
          <option value="fall">Fall 2024</option>
        </select>
        </div>
      <div class="form-item">
        <label for="residency">Residency</label>
        <select id="residency">
          <option value="INDIST">In District Student</option>
          <option value="OUTDIST">Out of District Student</option>
          <option value="OUTSTATE">Out of State Student</option>
          <option value="INTER">International Student</option>
        </select>
      </div>
      <div class="form-item">
        <label for="assochours">Level 100 & 200 Credit Hours</label>
        <input type="text" id="assochours">
      </div>
      <div class="form-item">
        <label for="bachhours">Level 300 & 400 Credit Hours</label>
        <input type="text" id="bachhours">
      </div>
      <label for="submit"></label>
      <input type="submit" id="submit" value="&nbsp;Calculate!&nbsp;">
    </fieldset>
  </form>
  <div id="tuition-calc-payment-info"></div>
ENDTEXT;
  return $output;
}
