<?php

/**
 * @file
 * Contains the Tution calculator form.
 *
 * @see tuitioncalc.module
 */

/**
 * Defines the Future for Frontliners calculation form.
 */
function tuitioncalc_form($form, &$form_state) {

  $form['semester'] = [
    '#type' => 'select',
    '#title' => t('Semester'),
    '#options' => [
      // '20/FA' => t('Fall 2020'),
      '21/WI' => t('Winter 2021'),
    ],
    '#required' => TRUE,
  ];

  $form['residency'] = [
    '#type' => 'select',
    '#title' => t('Residency'),
    '#options' => [
      'indist' => t('In-District'),
      'outdist' => t('Out-of-District'),
      'outstate' => t('Out-of-State'),
      'intl' => t('International'),
    ],
    '#required' => TRUE,
  ];

  $form["sections"] = [
    '#prefix' => '<table><tr><th>Subject</th><th>Number</th><th>Section</th></tr>',
    '#suffix' => '</table>',
    '#tree' => 'true',
  ];

  for ($i = 0; $i < 6; $i++) {
    $form["sections"][$i] = [
      '#prefix' => '<tr>',
      '#suffix' => '</tr>',
    ];

    $form["sections"][$i]["sec_subject"] = [
      '#prefix' => '<td>',
      '#type' => 'textfield',
      '#size' => 5,
      '#suffix' => '</td>',
    ];

    $form["sections"][$i]["sec_course_no"] = [
      '#prefix' => '<td>',
      '#type' => 'textfield',
      '#size' => 5,
      '#suffix' => '</td>',
    ];

    $form["sections"][$i]["sec_no"] = [
      '#prefix' => '<td>',
      '#type' => 'textfield',
      '#size' => 5,
      '#suffix' => '</td>',
    ];

  }

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Calculate Amount',
  ];

  if (!empty($form_state['values']['results'])) {

    $results = $form_state['values']['results'];

    $form['section_detail'] = [
      '#prefix' => t('<h2>Course Fees Detail</h2>'),
      '#theme' => 'table',
      '#header' => [t('Section'), t('Title'), t('Credit Hours'), t('Course Fee'), t('Excess Contact Hours Fee'), t('Book Fee')],
      '#rows' => $results['section_detail'],
    ];

    $form['fee_totals'] = [
      '#prefix' => t('<h2>Estimated Tuition and Fees</h2>'),
      '#theme' => 'table',
      '#header' => [t('Fee'), t('Cost')],
      '#rows' => [
        [t('Tuition'), number_format($results['tuition_total'], 2)],
        [t('Registration Fee'), number_format($results['registration_fee'], 2)],
        [t('Infrastructure Fee'), number_format($results['infrastructure_fee'], 2)],
        [t('Technology Fee'), number_format($results['technology_fee'], 2)],
        [t('Service Fee'), number_format($results['service_fee'], 2)],
        [t('Student Activity Fee'), number_format($results['activity_fee'], 2)],
        [t('<strong>Total Tuition and Mandatory Fees</strong>'), number_format($results['tuition_and_fees'], 2)],
        [t('Course Fees'), number_format($results['course_fees'], 2)],
        [t('Excess Contact Hours Fees'), number_format($results['excess_contact_hours_fees'], 2)],
        [t('Book Fees'), number_format($results['bookstore_fees'], 2)],
        [t('<strong>Total Estimated Tuition and Fees</strong>'), number_format($results['total_charges'], 2)],
      ],
    ];

  }

  return $form;
}

/**
 * Validates form entries.
 */
function tuitioncalc_form_validate($form, &$form_state) {

  $calc = TuitionCalcService::create();

  $sec_info = &$form_state['values']['course_section_info'];

  foreach ($form_state['values']['sections'] as $key => $section) {
    if (empty($section['sec_subject']) && empty($section['sec_course_no']) && empty($section['sec_no'])) {
      // Ignore lines that are entirely empty.
      break;

    }
    elseif (!empty($section['sec_subject']) && !empty($section['sec_course_no']) && !empty($section['sec_no'])) {

      // Add the semester ID to lookup values.
      $section['sec_term'] = $form_state['values']['semester'];

      if ($section_info = $calc->getSectionInfo($section)) {
        $sec_info[] = $section_info;
      }
      else {
        form_set_error("sections][$key",
          t('Could not find specified section %sub-%num-%sec.', [
            '%sub' => $section['sec_subject'],
            '%num' => $section['sec_course_no'],
            '%sec' => $section['sec_no'],
          ])
        );
      }
    }
    else {
      // Throw an error if a section row is only partially filled.
      form_set_error("sections][$key", t('Please provide all values for each course section.'));
    }
  }

}

/**
 * Defines the Future for Frontliners calculation form submit handler.
 */
function tuitioncalc_form_submit($form, &$form_state) {

  $form_state['values']['results'] = TuitionCalcService::create()
    ->calculateAllSections(
      $form_state['values']['residency'],
      $form_state['values']['semester'],
      $form_state['values']['course_section_info']
    );
  $form_state['rebuild'] = TRUE;
}
