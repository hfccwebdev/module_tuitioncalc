  /**
 * Calculate EZPay Payments based on semester.
 */
(function ($) {
  var rowClasses = function(someNumber){
    return (someNumber%2 == 0) ? 'odd' : 'even';
  };
  $(document).ready(function()
    {
        $('#tuition-calc').submit(function()
        {
            var term = $('#tuition-calc #term').val();
            var residency = $('#tuition-calc #residency').val();
            var schedule = new Array();
            var techfee;
            var serfee;
            var regfee;
            var infrfee;
            var actfee;

            if (term == "winter") {
              switch(residency) {
                case 'INDIST':
                  assoctuit = 111;
                  bachtuit = 200;
                  break;
                case 'OUTDIST':
                  assoctuit = 194;
                  bachtuit = 265;
                  break;
                case 'OUTSTATE':
                  assoctuit = 281;
                  bachtuit = 350;
                  break;
                case 'INTER':
                  assoctuit = 281;
                  bachtuit = 350;
                  break;
              }
              techfee = 4;
              serfee = 18;
              actfee = 2;
              regfee = 50;
              infrfee = 60;
            }

           if (term == "spsu") {
              switch(residency) {
                case 'INDIST':
                  assoctuit = 111;
                  bachtuit = 200;
                  break;
                case 'OUTDIST':
                  assoctuit = 194;
                  bachtuit = 265;
                  break;
                case 'OUTSTATE':
                  assoctuit = 281;
                  bachtuit = 350;
                  break;
                case 'INTER':
                  assoctuit = 281;
                  bachtuit = 350;
                  break;
              }
              techfee = 4;
              serfee = 18;
              actfee = 2;
              regfee = 50;
              infrfee = 60;
            }

            if (term == "fall") {
              switch(residency) {
                case 'INDIST':
                  assoctuit = 115.50;
                  bachtuit = 200;
                  break;
                case 'OUTDIST':
                  assoctuit = 202;
                  bachtuit = 265;
                  break;
                case 'OUTSTATE':
                  assoctuit = 292.50;
                  bachtuit = 350;
                  break;
                case 'INTER':
                  assoctuit = 292.50;
                  bachtuit = 350;
                  break;
              }
              techfee = 4;
              serfee = 18;
              actfee = 2;
              regfee = 50;
              infrfee = 60;
            }

            var assochours = isNaN(parseInt($('#tuition-calc #assochours').val())) ? 0 : parseInt($('#tuition-calc #assochours').val());
            var bachhours = isNaN(parseInt($('#tuition-calc #bachhours').val())) ? 0 : parseInt($('#tuition-calc #bachhours').val());

            var totalhours = assochours + bachhours;
            var tuitioncost = (assochours * assoctuit) + (bachhours * bachtuit);
            var techfeecost = totalhours * techfee;
            var serfeecost = totalhours * serfee;
            var actfeecost = totalhours * actfee;
            var esttotal = tuitioncost + regfee + infrfee + techfeecost + serfeecost + actfeecost;

            var output = '<h3>Estimated Tuition Cost</h3>';
            output += '<table>';
            output += '<tr class="even"><td><strong>Fee</strong></td><td><strong>Cost</strong></td></tr>';
            output += '<tr class="odd"><td>Tuition: </td><td> $' + tuitioncost.toFixed(2) + '</td></tr>';
            output += '<tr class="even"><td>Registration Fee: </td><td> $' + regfee.toFixed(2) + '</td></tr>';
            output += '<tr class="odd"><td>Infrastructure Fee: </td><td> $' + infrfee.toFixed(2) + '</td></tr>';
            output += '<tr class="even"><td>Technology Fee: </td><td> $' + techfeecost.toFixed(2) + '</td></tr>';
            output += '<tr class="odd"><td>Service Fee: </td><td> $' + serfeecost.toFixed(2) + '</td></tr>';

            if(actfeecost > 0) {
              output += '<tr class="even"><td>Student Activity Fee: </td><td> $' + actfeecost.toFixed(2) + '</td></tr>';
              output += '<tr class="odd"><td><strong>Estimated Total:</strong> </td><td> <strong>$' + esttotal.toFixed(2) + '</strong></td></tr>';
            } else {
              output += '<tr class="even"><td><strong>Estimated Total:</strong> </td><td> <strong>$' + esttotal.toFixed(2) + '</strong></td></tr>';
            }
            output += '</table>';
            $('#tuition-calc-payment-info').html(output);
            return false;
        });
    });
})(jQuery);
